package com.company.exressions;

import com.company.interfaces.Expression;
import com.company.interpreter.InterpreterEngine;

public class AddExpression implements Expression {
    private String expression;

    public AddExpression(String expression) {
        this.expression = expression;
    }

    @Override
    public int interpret(InterpreterEngine engine) {
        return engine.add(expression);
    }
}
