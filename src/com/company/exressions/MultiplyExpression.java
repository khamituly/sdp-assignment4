package com.company.exressions;

import com.company.interfaces.Expression;
import com.company.interpreter.InterpreterEngine;

public class MultiplyExpression implements Expression {
    private String exprssion;

    public MultiplyExpression(String exprssion) {
        this.exprssion = exprssion;
    }

    @Override
    public int interpret(InterpreterEngine engine) {
       return engine.multiply(exprssion);
    }
}
