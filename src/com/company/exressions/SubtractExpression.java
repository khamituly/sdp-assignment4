package com.company.exressions;

import com.company.interfaces.Expression;
import com.company.interpreter.InterpreterEngine;

public class SubtractExpression implements Expression {
   private String expression;

    public SubtractExpression(String expression) {
        this.expression = expression;
    }

    @Override
    public int interpret(InterpreterEngine engine) {
        return engine.substrfct(expression);
    }
}
