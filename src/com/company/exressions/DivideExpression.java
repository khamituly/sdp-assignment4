package com.company.exressions;

import com.company.interfaces.Expression;
import com.company.interpreter.InterpreterEngine;

public class DivideExpression implements Expression {
    private String expression;

    public DivideExpression(String expression) {
        this.expression = expression;
    }

    @Override
    public int interpret(InterpreterEngine engine) {
        return engine.divide(expression);
    }
}
