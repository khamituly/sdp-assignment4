package com.company;

import com.company.domain.Calculator;
import com.company.interpreter.InterpreterEngine;

public class Main {

    public static void main(String[] args) {

        Calculator calculator = new Calculator(new InterpreterEngine());

        System.out.println("Result:"+calculator.interpret("5*"
                                    +calculator.interpret("4-"
                                    +calculator.interpret("8/"
                                    +calculator.interpret("5+3")))));

    }
}
