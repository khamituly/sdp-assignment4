package com.company.domain;

import com.company.exressions.AddExpression;
import com.company.exressions.DivideExpression;
import com.company.exressions.MultiplyExpression;
import com.company.exressions.SubtractExpression;
import com.company.interfaces.Expression;
import com.company.interpreter.InterpreterEngine;

public class Calculator {
    private InterpreterEngine engine;

    public Calculator(InterpreterEngine engine) {
        this.engine = engine;
    }

    public int interpret(String input){
        Expression expression = null;

            if(input.contains("+")){
                expression = new AddExpression(input);
            }else if(input.contains("-")){
                expression = new SubtractExpression(input);
            }else if(input.contains("/")){
                expression = new DivideExpression(input);
            }else if(input.contains("*")){
                expression = new MultiplyExpression(input);
            }


            int result = expression.interpret(engine);
            System.out.println(input);
            return result;
    }


}
