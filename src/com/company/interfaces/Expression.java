package com.company.interfaces;

import com.company.interpreter.InterpreterEngine;

public interface Expression {
    public int interpret(InterpreterEngine engine);
}
